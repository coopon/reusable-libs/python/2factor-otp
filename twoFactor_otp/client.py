import logging
import requests

from twoFactor_otp.country_code import CountryCode
from twoFactor_otp.utils import convert_response
from twoFactor_otp.utils import prefix_country_code

class OTPClient:
    def __init__(self, auth_key, logger=None):
        self.auth_key = auth_key
        self.base_url = "https://2factor.in/API/V1"
        self.otp_endpoint = f"/{self.auth_key}"+"/SMS/{receiver}"
        self.verify_otp_endpoint = f"/{self.auth_key}"+"/SMS/VERIFY/{session_id}/{otp}"

        self.logger = logger
        if not self.logger:
            self.logger = logging.getLogger("2FACTOR CLIENT")

    def send_otp(self, receiver, **kwargs):
        """Request the Service to send OTP message to given number
        Args:
            receiver_number(str): 10 digit mobile number with country code of reciever

            sender(str, optional): the name to appear in SMS as sender
            otp (int, optional): the opt value to send, if not service will generate
            otp_length (int, optional): the length of otp. default 4, max 9
        Returns:
            a response object with status and status message
        """
        self.logger.debug(f"received request to send OTP to {receiver}")
        receiver = prefix_country_code(CountryCode.INDIA, receiver)

        otp_url = self.base_url + self.otp_endpoint.format(receiver=receiver) + "/AUTOGEN"

        if "otp" in kwargs:
            otp_url = otp_url.replace("AUTOGEN", str(kwargs.get("otp")))
            self.logger.info("using custom OTP")
            self.logger.debug(f"custom OTP: {kwargs.get('otp')}")

        if "template_name" in kwargs:
            otp_url = otp_url + f"/{template_name}"
            self.logger.info("using custom TEMPLATE")
            self.logger.debug(f"custom TEMPLATE: {kwargs.get('template_name')}")

        self.logger.info(f"sending request to 2factor.in")
        self.logger.debug(f"request URL: {otp_url}")

        service_response = requests.get(otp_url)
        self.logger.info(f"received response from 2factor.in")
        self.logger.debug(f"status: {service_response.status_code}")

        _response = convert_response(service_response)
        self.logger.info(_response.message) 
        return _response

    def resend_otp(self, receiver, **kwargs):
        """Resend OTP request
        Args:
            receiver(str): 10 digit mobile number with country code of receiver
        Returns:
            a response object with status and status message
        """
        receiver = prefix_country_code(CountryCode.INDIA, receiver)
        _response = self.send_otp(receiver, **kwargs)
        return _response

    def verify_otp(self, session_id, otp_value):
        """Request to verify OTP with given mobile number
        Args:
            otp_value(int): the otp value to verify against
            session_id (str): value returned by 2factor.in while sending otp
                              this can be found in response.message
        Returns:
             a response object with status and status message
        """
        verify_url = self.base_url + self.verify_otp_endpoint.format(session_id=session_id, otp=otp_value)
        service_response = requests.get(verify_url)
        _response = convert_response(service_response)
        return _response
