class ServiceResponse:
    def __init__(self, status, message):
        self.status = status
        self.message = message

    def __repr__(self):
        return f"<ServiceResponse: {self.status}:{self.message}>"
