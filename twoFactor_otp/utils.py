from twoFactor_otp.country_code import CountryCode
from twoFactor_otp.exceptions import InvalidCountryCode
from twoFactor_otp.response import ServiceResponse

def prefix_country_code(country_code, number):
    """Prefix country code to phone number
    Args:
        country_code: the country code to prefix of type CountryCode
        number: the mobile number
    Returns:
        number: mobile number with country code prefixed. if country_code
        is already prefixed, then the same is returned unaltered.
    """
    if not isinstance(country_code, CountryCode):
        raise InvalidCountryCode("The country code should be of type CountryCode")
    if len(number) == 10 and (not number.startswith(country_code.value)):
            number = country_code.value + number
    return number

def convert_response(response_obj):
    """Convert requests package response obj to our internal Response
    Args:
        response_obj: the response object returned by requests package
    Returns:
        our interal Response object
    """
    resp_status = response_obj.json().get("Status")
    message = response_obj.json().get("Details")

    # default assuming the request was successful
    status_code = 200

    if resp_status == "Error":
        status_code = 400

    _response = ServiceResponse(status_code, message)
    return _response
