### 2factor-otp

[2factor.in](https://2factor.in) is one of the bulk SMS provider in India. This package `2factor-otp` is a python wrapper for **sending** and **verifying** OTPs using 2factor.in. This is a minimal wrapper covers only OTP use-case and doesn't cover all HTTP API's exposed by 2factor.

#### Install

    python setup.py install

#### Usage

    from 2factor_otp.client import OTPClient
    
    # obtain this auth_key from 2factor.in
    auth_key = "xxxxxxxxxxxxxxxxx"
    otp_client = OTPClient(auth_key)
    
    # if OTP value is not supplied, 2factor automatically creates for you
    receiver = "9876543210"
    service_response = otp_client.send_otp(receiver)

    # if you want to supply your own OTP
    otp = 3342
    service_response = otp_client.send_otp(receiver, otp=otp)

    print (service_response.status, service_response.message)

    # verify OTP for a given mobile number
    verify_response = otp_client.verify_otp(service_response.message, otp)
    print (verify_response.status, verify_response.message)

    # re-send a OTP
    otp_resend = otp_client.resend_otp(receiver)
    print (otp_resend.status, otp_resend.message)


#### Contributors

1. Prasanna Venkadesh

#### License

This package is licenced under GNU AGPL v3.0
