import setuptools
from setuptools import find_packages

setuptools.setup(
    name="2factor-otp",
    version="0.0.1",
    author="Prasanna Venkadesh",
    author_email="prasanna@cooponscitech.in",
    description="Minimal OTP only API coverage for 2factor.in service",
    python_requires='>=2.7',
    install_requires=["requests==2.22.0"],
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    url="",
    classifiers=[
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: Implementation :: CPython',

        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Operating System :: OS Independent",
    ]
)

