import pytest
from requests import Response

from twoFactor_otp.country_code import CountryCode
from twoFactor_otp.exceptions import InvalidCountryCode
from twoFactor_otp.utils import prefix_country_code, convert_response
from twoFactor_otp.response import ServiceResponse

test_numbers = ("9876543210", "+919876543210")

def test_valid_prefix_country_code():
    """Test valid country code"""
    india_code = CountryCode.INDIA
    for number in test_numbers:
        test_output = prefix_country_code(india_code, number)
        assert len(test_output) == 13
        assert india_code.value in test_output


def test_invalid_prefix_country_code():
    """Test invalid country code.

    Should raise InvalidCountryCode exception
    """
    country_code = "+91"
    with pytest.raises(InvalidCountryCode):
        for number in test_numbers:
            test_output = prefix_country_code(country_code, number)


def test_error_convert_response():
    """Test for failed service request"""
    mock_response_state = {
            "_content": b'{"Status": "Error", "Details": "failure"}',
            "headers": {"content-type": "application/json"}
            }
    mock_response_obj = Response()
    mock_response_obj.__setstate__(mock_response_state)
    test_output = convert_response(mock_response_obj)
    assert isinstance(test_output, ServiceResponse) == True
    # test object attributes
    assert hasattr(test_output, "status") == True
    assert hasattr(test_output, "message") == True
    # test object attribute values
    assert test_output.status == 400
    assert test_output.message == "failure"


def test_success_convert_response():
    """Test for successful service request"""
    mock_response_state = {
            "_content": b'{"Status": "success", "Details": "passed"}',
            "headers": {"content-type": "application/json"}
            }
    mock_response_obj = Response()
    mock_response_obj.__setstate__(mock_response_state)
    test_output = convert_response(mock_response_obj)
    assert isinstance(test_output, ServiceResponse) == True
    # test object attributes
    assert hasattr(test_output, "status") == True
    assert hasattr(test_output, "message") == True
    # test object attribute values
    assert test_output.status == 200
    assert test_output.message == "passed"
